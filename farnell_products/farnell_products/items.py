# -*- coding: utf-8 -*-

"""
.. module:: .items
   :synopsis: Define here the models for your scraped items
   See documentation in:
   http://doc.scrapy.org/en/latest/topics/items.html
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

import scrapy


class FarnellProductsItem(scrapy.Item):
    """
    Farnell product item model
    """

    # (string) Url of the product being scraped
    url = scrapy.Field()

    # (string) Product Brand Name
    brand = scrapy.Field()

    # (string) Product headline containing the brand and product name
    title = scrapy.Field()

    # (float) Unit price of the product
    unit_price = scrapy.Field()

    # (string) Product Overview description, usually has advertising copy
    overview = scrapy.Field()

    # (string) Dict array of specification
    # objects in {name: specname, value: specvalue}
    information = scrapy.Field()

    # (string) Manufacturer
    manufacturer = scrapy.Field()

    # (string) Manufacturer part number
    manufacturer_part = scrapy.Field()

    # (string): Tariff code/number
    tariff_number = scrapy.Field()

    # (string): Origin country
    origin_country = scrapy.Field()

    # (string list) String array of
    # "Technical Docs" filenames (usually PDF or URL titles)
    files = scrapy.Field()

    # (string list) String array of "Technical Docs" URLs
    file_urls = scrapy.Field()

    # (string list) String Array of additional image urls
    image_urls = scrapy.Field()

    # (string) URL of the main product image
    primary_image_url = scrapy.Field()

    # (string list) Ordered string array
    # (highest level category first) of categories
    trail = scrapy.Field()
