# -*- coding: utf-8 -*-

"""
.. module:: .__init__.py
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

import os

from scrapy.http import (Request, HtmlResponse)


def fake_response(file_name, url=None):
    """
    Fakes response by creating Scrapy response
    @param file_name: file
    @param url: url
    """

    if not url:
        url = 'http://www.example.com'

    request = Request(url=url)
    if not file_name[0] == '/':
        responses_dir = os.path.dirname(os.path.realpath(__file__))
        file_path = os.path.join(responses_dir, file_name)
    else:
        file_path = file_name

    with open(file_path, 'r', encoding="utf8") as fh:
        file_content = fh.read()

    response = HtmlResponse(
        url=url,
        request=request,
        encoding='utf-8',
        body=file_content
    )

    return response
