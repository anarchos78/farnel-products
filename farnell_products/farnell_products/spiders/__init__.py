# -*- coding: utf-8 -*-

"""
.. module:: .__init__
   :synopsis:
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from ..spiders.products import ProductsSpider

__all__ = ['ProductsSpider']
