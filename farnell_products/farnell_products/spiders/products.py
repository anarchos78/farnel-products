# -*- coding: utf-8 -*-

"""
.. module:: .products
   :synopsis: Farnell product spider
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

import scrapy
import re

from re import sub
from decimal import Decimal
from bs4 import BeautifulSoup

from ..items import FarnellProductsItem


class ProductsSpider(scrapy.Spider):
    """
    Farnell product spider
    """
    name = "products"
    allowed_domains = ["uk.farnell.co.uk"]
    start_urls = ["http://uk.farnell.com/browse-for-products/"]

    def parse(self, response):
        """
        The entry point of spider's parsing chain
        """

        # Get the main category urls links to follow
        category_urls = response.xpath(
            "//div[@class='categoryContainer']"
            "//div[@class='categoryList']//h2/a/@href"
        ).extract()

        # Follow category urls
        for category_url in category_urls:
            yield response.follow(
                category_url,
                self._follow_category_url,
                dont_filter=True
            )

    def _follow_category_url(self, response):
        """
        Parses category url to fetch category's product list
        """

        product_list_url = response.xpath(
            "//a[@class='showAllProductsBottom']//@href"
        ).extract_first()

        # Follow all products url
        yield response.follow(
            product_list_url,
            self._parse_product_list,
            dont_filter=True
        )

    def _parse_product_list(self, response):
        """
        Parses product list
        """

        # Product links
        product_links = response.xpath(
            "//td[@class='productImage mftrPart']/a/@href"
        ).extract()

        # Follow the product link to scrape it
        for product_link in product_links:
            yield response.follow(
                product_link,
                self._parse_product_page,
                dont_filter=True
            )

        # Follow the paginated links if any found
        for page in response.xpath(
                "//div[@class='paginNext pageLink']/a/@href"):
            yield response.follow(
                page,
                self._parse_product_list,
                dont_filter=True
            )

    @staticmethod
    def _get_product_info(response):
        """Get product's info"""

        soup = BeautifulSoup(
            response.body,
            'html.parser',
            from_encoding='utf-8'
        )

        # It might be a case that "Product Information" section wouldn't exist
        try:
            information_header = soup.find_all(
                'h2', text=re.compile('Product Information')
            )[0].parent
        except IndexError:
            return 'N/A'

        prod_info_header_parent_element = information_header.findNext(
            'div', attrs={'class': 'collapsable-content'}
        )

        keys = [
            dt.text.strip().replace(':', '')
            for dt in prod_info_header_parent_element.find_all('dt')
        ]
        values = [
            dd.text.strip()
            for dd in prod_info_header_parent_element.find_all('dd')
        ]

        return str(dict(zip(keys, values)))

    @staticmethod
    def _get_legislation_section(response):
        """Get product's legislation information"""

        soup = BeautifulSoup(
            response.body,
            'html.parser',
            from_encoding='utf-8'
        )

        legislation_section = soup.find(
            'section', attrs={'id': 'pdpSection_ProductLegislation'}
        )

        dl = legislation_section.findNext('dl')

        keys = [
            dt.text.strip().replace(':', '').lower()
            for dt in dl.find_all('dt')
        ]
        values = [
            dd.contents[0].strip()
            for dd in dl.find_all('dd')
        ]

        return dict(zip(keys, values))

    @staticmethod
    def _get_product_description(response):
        """Get product's description"""

        soup = BeautifulSoup(
            response.body,
            'html.parser',
            from_encoding='utf-8'
        )

        description_section = soup.find(
            'div', attrs={'class': 'productDescription packaging'}
        )

        dl = description_section.findNext('dl')

        keys = [
            dt.text.strip().replace(':', '').lower()
            for dt in dl.find_all('dt')
        ]
        values = [
            dd.text.strip()
            for dd in dl.find_all('dd')
        ]

        product_description = dict(zip(keys, values))

        # There is a case the we get 2 "manufacturer" strings that are the same
        try:
            manufacturer = product_description.get('manufacturer')
            if manufacturer:
                product_description['manufacturer'] = manufacturer.split('\n', 1)[1]
        except IndexError:
            pass

        return product_description

    @staticmethod
    def _get_technical_docs(response):
        """Get product's technical docs"""

        soup = BeautifulSoup(
            response.body,
            'html.parser',
            from_encoding='utf-8'
        )

        technical_docs_section = soup.find(
            'section', attrs={'id': 'techDocsHook'}
        )

        doc_titles = [
            link.text.strip()
            for link in technical_docs_section.find_all('a')
        ]

        doc_links = [
            link.attrs.get('href')
            for link in technical_docs_section.find_all('a')
        ]

        technical_docs = {
            'links': ', '.join(doc_links),
            'titles': ', '.join(doc_titles)
        }

        return technical_docs

    @staticmethod
    def _get_media(response):
        """Get product's images"""

        soup = BeautifulSoup(
            response.body,
            'html.parser',
            from_encoding='utf-8'
        )

        primary_image_url = soup.find(
            'img', attrs={'id': 'productMainImage'}
        ).get('src')

        thumbnail_container = soup.find(
            'div', attrs={'id': 'thumbsContainer'}
        )

        # There is a case the the spider returns a relative url like:
        # /wcsstore7.00.17.860.5/SafeB2BStorefrontAssetStore/images/360icon.png
        # We suspect that the relative url is for "360 view" thumbnail
        # and not for an actual image.
        # In our case we need the absolute urls.
        thumbnail_urls = [
            img.get('src')
            for img in thumbnail_container.findAll('img')
            if 'http://' in img.get('src')
        ]

        images = {
            'primary': primary_image_url,
            'thumbs': ', '.join(thumbnail_urls)
        }

        return images

    @staticmethod
    def _get_trail(response):
        """Get product's categories-subcategories"""

        soup = BeautifulSoup(
            response.body,
            'html.parser',
            from_encoding='utf-8'
        )

        breadcrumb = soup.find(
            'div', attrs={'id': 'breadcrumb'}
        )

        breadcrumb_parts = [
            part.text.strip().replace('\xa0', '').replace('>', '')
            for part in breadcrumb.findAll('li')
        ]

        return ','.join(breadcrumb_parts[1:-1])

    def _parse_product_page(self, response):
        """
        Parses product page
        """

        item = FarnellProductsItem()

        # Url of the product being scraped
        url = response.url

        # Product Brand Name
        product_description = self._get_product_description(response)
        brand = product_description.get('manufacturer', 'N/A')

        # Product headline containing the brand and product name
        title = response.xpath(
            "normalize-space(concat("
            "//div[@class='mainPdpWrapper']//h1//text(), "
            "//div[@class='mainPdpWrapper']//h2/span//text()))"
        ).extract_first(default='N/A')

        # Unit price of the product
        unit_price_str = response.xpath(
            "normalize-space(//div[@class='productPrice']//span[1]//text())"
        ).extract_first(default=0.0)

        # Covers the cases of: empty string, string integer, string float etc
        unit_price = float(
            Decimal(
                sub('[^\d.]', '', unit_price_str if unit_price_str else '0.0')
            )
        )

        # Product Overview description, usually has advertising copy
        overview = response.xpath(
            "normalize-space(//div[@class='heading collapsable-trigger' and "
            "contains(., 'Product Overview')]//following-sibling::div)"
        ).extract_first(default='N/A')

        # Dict array of specification
        # objects in {name: specname, value: specvalue}
        information = self._get_product_info(response)

        # Manufacturer
        manufacturer = product_description.get('manufacturer', 'N/A')

        # Manufacturer part number
        manufacturer_part = product_description.get(
            'manufacturer part no', 'N/A'
        )

        # Tariff code/number
        legislation_info = self._get_legislation_section(response)
        tariff_number = legislation_info.get('tariff no', 'N/A')

        # Origin country
        origin_country = legislation_info.get('country of origin', 'N/A')

        # String array of "Technical Docs"
        # filenames (usually PDF or URL titles)
        technical_docs = self._get_technical_docs(response)
        files = technical_docs.get('titles', 'N/A')

        # String array of "Technical Docs" URLs
        file_urls = technical_docs.get('links', 'N/A')

        # String Array of additional image urls
        media = self._get_media(response)
        image_urls = media.get('thumbs', 'N/A')

        # URL of the main product image
        primary_image_url = media.get('primary', 'N/A')

        # Ordered string array (highest level category first) of categories
        trail = self._get_trail(response)

        # Replace non-breaking space with space ('\xa0' with ' ')
        item['url'] = url
        item['brand'] = brand
        item['title'] = title.replace('\xa0', ' ')
        item['unit_price'] = unit_price
        item['overview'] = overview.replace(u'\xa0', u' ')
        item['information'] = information
        item['manufacturer'] = manufacturer
        item['manufacturer_part'] = manufacturer_part
        item['tariff_number'] = tariff_number
        item['origin_country'] = origin_country
        item['files'] = files
        item['file_urls'] = file_urls
        item['image_urls'] = image_urls
        item['primary_image_url'] = primary_image_url
        item['trail'] = trail

        return item
