# -*- coding: utf-8 -*-

"""
.. module:: .test_product
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

import unittest

from farnell_products.farnell_products.items import FarnellProductsItem
from ..farnell_products.spiders import ProductsSpider
from .responses import fake_response


class TestProductsSpider(unittest.TestCase):
    """Tests ProductsSpider spider"""

    def setUp(self):
        self.spider = ProductsSpider()

        self.categories = fake_response(
            'html/browse-for-products.html'
        )
        self.category = fake_response(
            'html/engineering-software.html'
        )
        self.product_list = fake_response(
            'html/engineering-software-list-results.html'
        )
        self.product = fake_response('html/product-2309447.html')

    def test_parse(self):
        """Test parse method"""

        response = self.spider.parse(self.categories)

        category_urls = [
            a.__dict__.get('_url')
            for a in response
        ]

        # We know that there are 28 categories
        self.assertEqual(len(category_urls), 28)

    def test_follow_category_url(self):
        """Test _follow_category_url method"""

        product_url_list = self.spider._follow_category_url(self.category)

        category_url = [
            a.__dict__.get('_url')
            for a in product_url_list
        ]

        # We know the dummy page contains the show all products link:
        # http://uk.farnell.com/c/engineering-software/prl/results
        self.assertEqual(
            'http://uk.farnell.com/c/engineering-software/prl/results',
            category_url[0]
        )

    def test_parse_product_list(self):
        """Test _parse_product_list method"""

        product_list = self.spider._parse_product_list(self.product_list)

        product_links = [link for link in product_list]

        # The results are paginated, we expect 25 product links and 1 link
        # that points to the next page, so 26 links in total
        self.assertEqual(len(product_links), 26)

    def test_get_product_info(self):
        """Test _get_product_info method"""

        product_info = self.spider._get_product_info(self.product)

        # Type check
        self.assertIsInstance(product_info, str)

        # Do we have the information
        self.assertIn('Core Architecture', product_info)
        self.assertIn('ARM7, ARM9, Cortex-M, PIC32', product_info)
        self.assertIn('Supported Families', product_info)

    def test_get_legislation_section(self):
        """Test _get_legislation_section method"""

        product_info = self.spider._get_legislation_section(self.product)

        # Type check
        self.assertIsInstance(product_info, dict)

        # Do we have the information
        self.assertIn('country of origin', product_info)
        self.assertIn('United States', product_info.values())

        self.assertIn('tariff no', product_info)
        self.assertIn('85238091', product_info.values())

    def test_get_product_description(self):
        """Test _get_product_description method"""

        product_description = self.spider._get_product_description(
            self.product
        )

        # Type check
        self.assertIsInstance(product_description, dict)

        # Do we have the information
        self.assertIn('manufacturer', product_description)
        self.assertIn('SEGGER', product_description.values())

    def test_get_technical_docs(self):
        """Test _get_technical_docs method"""

        product_technical_docs = self.spider._get_technical_docs(
            self.product
        )

        # Type check
        self.assertIsInstance(product_technical_docs, dict)

        # Do we have the information
        self.assertIn('links', product_technical_docs)
        self.assertIn('titles', product_technical_docs)
        self.assertIn(
            'http://www.farnell.com/datasheets/1699433.pdf, '
            'http://www.element14.com/community/docs/DOC-46721?'
            'z=qL2YJg?CMP=PRR-EUR-13-0015',
            product_technical_docs.values()
        )

    def test_get_media(self):
        """Test _get_media method"""

        product_media = self.spider._get_media(self.product)

        # Type check
        self.assertIsInstance(product_media, dict)

        # Do we have the information
        self.assertIn('primary', product_media)
        self.assertIn(
            'http://uk.farnell.com/productimages/'
            'standard/en_GB/2293173-40.jpg',
            product_media.values()
        )

        self.assertIn('thumbs', product_media)
        self.assertIsNone(None, product_media.get('thumbs'))

    def test_get_trail(self):
        """Test _get_trail method"""

        breadcrumb = self.spider._get_trail(self.product)

        # Type check
        self.assertIsInstance(breadcrumb, str)

        # Do we have the information
        self.assertEqual(
            'Engineering Software,OS & Middleware,Embedded Operating Systems',
            breadcrumb
        )

    def test_parse_product_page(self):
        """Test _parse_product_page method"""

        item = self.spider._parse_product_page(self.product)

        # Type check
        self.assertIsInstance(item, FarnellProductsItem)

        # Do we have the information
        self.assertIn('manufacturer', item)
        self.assertIn('SEGGER', item.values())

        self.assertIn('file_urls', item)
        self.assertIn(
            'http://www.farnell.com/datasheets/1699433.pdf, '
            'http://www.element14.com/community/docs/DOC-46721'
            '?z=qL2YJg?CMP=PRR-EUR-13-0015',
            item.values()
        )

        self.assertIn('trail', item)
        self.assertIn(
            'Engineering Software,OS & Middleware,Embedded Operating Systems',
            item.values()
        )

        self.assertIn('url', item)
        self.assertIn('http://www.example.com', item.values())
