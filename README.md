# Farnell Project: Product spider #

PY3 Scrapy spider to scrape product information from uk.farnell.com.

### Product spider ###

The spider extracts information about every farnell store product.

Fields:

* url: (string) Url of the product being scraped
* brand: (string) Product Brand Name
* title (string) Product headline containing the brand and product name
* unit_price: (float) Unit price of the product
* overview: (string) Product Overview description, usually has advertising copy
* information: (string) Dict array of specification objects in {name: specname, value: specvalue}
* manufacturer: (string) Manufacturer
* manufacturer_part: (string) Manufacturer part number
* tariff_number (string): Tariff code/number
* origin_country (string): Origin country
* files: (string list) String array of "Technical Docs" filenames (usually PDF or URL titles)
* file_urls: (string list) String array of "Technical Docs" URLs
* image_urls: (string list) String Array of additional image urls
* primary_image_url: (string) URL of the main product image
* trail: (string list) Ordered string array (highest level category first) of categories

The spider is deployable on Scrapy Cloud platform (https://dash.scrapinghub.com).
