# -*- coding: utf-8 -*-

"""
.. module:: .main
   :synopsis: 
.. moduleauthor:: Athanasios Rigas <admin@ithemis.gr>

"""

from scrapy import cmdline

cmdline.execute("scrapy crawl products".split())
